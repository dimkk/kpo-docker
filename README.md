# Docker repo for kph #

### Quick setup ###

* [Download docker toolbox](https://www.docker.com/products/docker-toolbox)
* Open Docker QuickStart Terminal (wait for docker-machine to be up, remember IP)
* Ask me to share with you internal docker images from docker hub
* No git? Install from [here](https://git-scm.com/download)
* Switch to Docker QuickStart Terminal
* git clone https://bitbucket.org/dimkk/kpo-docker.git
* cd kpo-docker
* docker-compose up
* now you got running instances of, frontend on 192.168.99.100:8088, backend on 192.168.99.100:8080