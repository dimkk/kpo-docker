FROM node:latest

MAINTAINER  dimkk


COPY ../back /var/www/kpo
WORKDIR /var/www/kpo

RUN npm install

EXPOSE 8080

ENTRYPOINT ["npm", "start"]

# To build:
# docker build -f docker-node-codewithdan.dockerfile --tag codewithdan_node ../

# To run:
# docker run -d -p 8080:8080 -v $(PWD):/var/www/codewithdan -w /var/www/codewithdan codewithdan_node
# docker run -d -p 8080:8080 --name codewithdan_node codewithdan_node 
